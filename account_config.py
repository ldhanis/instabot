username = "instagram account name"
password = "instagram account password"

pause = 60 * 30

hashtags = 	['Porsche', 'porschecollector', 'germancars',
			'PorscheMoment',
			'Porsche911', '911', 'travelbloggers',
			'Porsche964', 'PorscheGT',
			'PorscheGT3RS', 'Cars', 'Turbo',
			'Panamera',
			'Spyder', 'Targa', 'PorscheTarga', 'PorschePanamera',
			'Instacar',
			'instacar', 'instaporsche', 'carsofinstagram', 'carrera',
			'porschelife',
			'supercar', 'porscheclassic', 'carlifestyle', 'amazingcars',
			'racing', 'follow4follow', 'like4like', 'porsche_army']

comments = [u'What an amazing shot! :heart_eyes: What do '
			u'you think of my recent shot?',
			u'What an amazing shot! :heart_eyes: I think '
			u'you might also like mine. :wink:',
			u'Wonderful!! :heart_eyes: Would be awesome if '
			u'you would checkout my photos as well!',
			u'Wonderful!! :heart_eyes: I would be honored '
			u'if you would checkout my images and tell me '
			u'what you think. :wink:',
			u'This is awesome!! :heart_eyes: Any feedback '
			u'for my photos? :wink:',
			u'This is awesome!! :heart_eyes:  maybe you '
			u'like my photos, too? :wink:',
			u'I really like the way you captured this. I '
			u'bet you like my photos, too :wink:',
			u'I really like the way you captured this. If '
			u'you have time, check out my photos, too. I '
			u'bet you will like them. :wink:',
			u'Great capture!! :smiley: Any feedback for my '
			u'recent shot? :wink:',
			u'Great capture!! :smiley: :thumbsup: What do '
			u'you think of my recent photo?']
