#! /usr/bin/python3

import time, os, schedule
from instapy import InstaPy

from account_config import username, password, hashtags, comments, pause
from routines import discover_accounts, beginner_routine

def generate_local_session():
    return InstaPy(username=username, password=password, disable_image_load=True, headless_browser=True)

def job():
    discover_accounts(generate_local_session(), hashtags, comments)

try:
    job()
except:
    os.system('sudo shutdown -r now')

os.system('sudo shutdown -r now')
