Bot Instagram:

Le bot fonctionne sur base de trafic organique (trafic réel instagram) et non de faux comptes qui viendraient suivre le profil ou aimer / commenter les publications.
Il est développé en python grâce à une librairie appelée InstaPy.
Pour son fonctionnement, le bot a besoin d’informations de base : 
1.	Une liste de hashtags en rapport avec le contenu du compte connecté au bot (idéalement une quinzaine)
2.	Une liste de hashtags avec lesquels on souhaite ne pas réagir (plus il y en a, plus le trafic généré sera pertinent)
3.	Une liste de commentaires à poster en rapport avec le contenu du compte connecté au bot (il en faut une bonne dizaine, qui soient assez génériques pour être postés sur beaucoup de contenu différent mais en rapport avec les hashtags choisis)
Par exemple, dans le cas d’un compte lié à la marque Porsche on aura :
1.	Porsche, Flat6, SportCars, HorsePower, SuperCar, …
2.	BadMood, Rain, …
3.	« Wow, that’s a nice car ! », « Nice picture, come check mine ! », « What a beast ! I’m sure you will like my cars ! », …
Le bot fonctionne comme suit : 
-	Il va récupérer le contenu du fil d’actualité (60 publications) d’un hashtag pris au hasard dans la liste 1 (donc les 60 dernières publications contenant ce hashtag)
-	Il va récupérer le nombre d’abonnés et le nombre d’abonnement de chaque compte ayant posté une vidéo / photo dans ce fil d’actualité et calculer le social rate de chaque compte : abonnés / abonnements.
-	Avant de vérifier si le social rate nous intéresse, on regarde si le compte a trop d’abonnés pour nous intéresser (si un compte a au-delà de 8000 abonnés, il ne nous intéresse pas car trop « populaire » et risquerait d’être un bot instagram. Ce que l’on veut c’est du trafic organique et pas artificiel.
-	Si un compte a un social rate supérieur à 1,5 cela signifie que le compte est « trop » populaire et que donc il ne s’abonnera pas en retour, ou qu’il supprimera son abonnement quelques heures après avoir commencé à nous suivre. En deçà de 0,5 cela signifie que le compte est un compte trop peu populaire et que donc il n’intéragit pas assez avec Instagram.
La tranche qui intéresse le bot est donc entre 0,5 et 1,5.
-	Après avoir regardé le nombre d’abonnés et le social rate, si un compte entre dans nos critères, le bot va s’abonner au nom du compte qu’il dirige et aimer la dernière publication. Si cette publication a un taux de conversion (commentaires / likes) trop élevé (au-delà de 0,5) ou que la publication a trop de commentaires (au-delà de 10), on ne va pas commenter sinon notre commentaire se retrouverait noyé au milieu des autres.
-	Une fois cela fini avec chaque compte des 60 dernières publications d’un hashtag, le bot va faire la même opération avec un autre hashtag de la liste.
Le bot a un « quota superivsor » qui permet de ne pas se faire bannir pour utilisation de bot (ou botting / scripting). Ce quota supervisor regarde le nombre d’abonnements faits au nom de la page et le nombre de commentaires, si le bot a écrit plus de 250 commentaires sur une journée ou s’est abonné à plus de 400 personnes, il prend un break aléatoire entre 5 et 9 heures.
Une fois cette pause prise, le bot va se désabonner de tous les abonnements qu’il a fait (pour optimiser le social rate).
Si les commentaires sont assez génériques, les utilisateurs (que le bot a suivi) ayant posté les publications sous lesquelles le bot a interagi viendront s’abonner au compte géré par le bot (ça s’appelle un followback). Un bot bien géré peut espérer une quarantaine d’abonnements journaliers.
Il est important de souvent publier (environ une ou deux photos par semaine) pour créer de l’interaction avec la communauté naissante du compte.

