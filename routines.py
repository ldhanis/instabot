import random
from instapy import InstaPy
from instapy.util import smart_run

def discover_accounts(session, hashtags, comments):
	with smart_run(session):

		random.shuffle(hashtags)
		my_hashtags = hashtags[:10]

		# general settings
		session.set_do_follow(enabled=True, percentage=40, times=1)
		session.set_do_comment(enabled=True, percentage=20)
		session.set_comments(comments)
		session.set_do_like(True, percentage=70)
		session.set_delimit_liking(enabled=True, max=100, min=0)
		session.set_delimit_commenting(enabled=True, max=20, min=0)
		session.set_relationship_bounds(enabled=True,
										potency_ratio=None,
										delimit_by_numbers=True,
										max_followers=3000,
										max_following=2000,
										min_followers=100,
										min_following=50)

		session.set_quota_supervisor(enabled=True,
									sleep_after=["likes", "follows"],
									sleepyhead=True, stochastic_flow=True,
									notify_me=True,
									peak_likes=(100, 1000),
									peak_comments=(21, 250),
									peak_follows=(200, None))

		session.set_user_interact(amount=1, randomize=False, percentage=40)

		# activity
		session.like_by_tags(my_hashtags, amount=60, media=None)
		session.unfollow_users(amount=500, InstapyFollowed=(True, "nonfollowers"),
							   style="FIFO",
							   unfollow_after=12 * 60 * 60, sleep_delay=501)
		session.unfollow_users(amount=500, InstapyFollowed=(True, "all"),
							   style="FIFO", unfollow_after=24 * 60 * 60,sleep_delay=501)

def beginner_routine(session, comments):
	with smart_run(session):
		# settings
		session.set_user_interact(amount=3, randomize=True, percentage=100, media='Photo')
		session.set_relationship_bounds(enabled=True,
										potency_ratio=None,
										delimit_by_numbers=True,
										max_followers=3000,
										max_following=900,
										min_followers=50,
										min_following=50)
		session.set_simulation(enabled=False)
		session.set_do_like(enabled=True, percentage=100)
		session.set_ignore_users([])
		session.set_do_comment(enabled=True, percentage=35)
		session.set_do_follow(enabled=True, percentage=25, times=1)
		session.set_comments(comments)
		session.set_ignore_if_contains([])
		session.set_action_delays(enabled=True, like=40)

		# activity
		session.interact_user_followers([], amount=340)
